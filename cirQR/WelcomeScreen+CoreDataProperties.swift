//
//  WelcomeScreen+CoreDataProperties.swift
//  cirQR
//
//  Created by Mustafa Sabur on 09/11/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import Foundation
import CoreData


extension WelcomeScreen {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WelcomeScreen> {
        return NSFetchRequest<WelcomeScreen>(entityName: "WelcomeScreen");
    }

    @NSManaged public var media: NSData?
    @NSManaged public var text: String?
    @NSManaged public var nextScreen: WelcomeScreen?
    @NSManaged public var product: Product?

}
