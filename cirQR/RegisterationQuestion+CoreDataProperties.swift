//
//  RegisterationQuestion+CoreDataProperties.swift
//  cirQR
//
//  Created by Mustafa Sabur on 09/11/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import Foundation
import CoreData


extension RegisterationQuestion {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RegisterationQuestion> {
        return NSFetchRequest<RegisterationQuestion>(entityName: "RegisterationQuestion");
    }

    @NSManaged public var question: String?
    @NSManaged public var product: Product?

}
