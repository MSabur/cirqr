//
//  Product+CoreDataProperties.swift
//  cirQR
//
//  Created by Mustafa Sabur on 09/11/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import Foundation
import CoreData


extension Product {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product");
    }

    @NSManaged public var brand: String?
    @NSManaged public var mainImage: NSData?
    @NSManaged public var model: String?
    @NSManaged public var name: String?
    @NSManaged public var thumbnail: NSData?
    @NSManaged public var upc: Int64
    @NSManaged public var warrantyDuration: Int32
    @NSManaged public var firstWelcomeScreen: NSSet?
    @NSManaged public var registerationQuestion: NSSet?
    @NSManaged public var userProduct: NSSet?

}

// MARK: Generated accessors for firstWelcomeScreen
extension Product {

    @objc(addFirstWelcomeScreenObject:)
    @NSManaged public func addToFirstWelcomeScreen(_ value: WelcomeScreen)

    @objc(removeFirstWelcomeScreenObject:)
    @NSManaged public func removeFromFirstWelcomeScreen(_ value: WelcomeScreen)

    @objc(addFirstWelcomeScreen:)
    @NSManaged public func addToFirstWelcomeScreen(_ values: NSSet)

    @objc(removeFirstWelcomeScreen:)
    @NSManaged public func removeFromFirstWelcomeScreen(_ values: NSSet)

}

// MARK: Generated accessors for registerationQuestion
extension Product {

    @objc(addRegisterationQuestionObject:)
    @NSManaged public func addToRegisterationQuestion(_ value: RegisterationQuestion)

    @objc(removeRegisterationQuestionObject:)
    @NSManaged public func removeFromRegisterationQuestion(_ value: RegisterationQuestion)

    @objc(addRegisterationQuestion:)
    @NSManaged public func addToRegisterationQuestion(_ values: NSSet)

    @objc(removeRegisterationQuestion:)
    @NSManaged public func removeFromRegisterationQuestion(_ values: NSSet)

}

// MARK: Generated accessors for userProduct
extension Product {

    @objc(addUserProductObject:)
    @NSManaged public func addToUserProduct(_ value: UserProduct)

    @objc(removeUserProductObject:)
    @NSManaged public func removeFromUserProduct(_ value: UserProduct)

    @objc(addUserProduct:)
    @NSManaged public func addToUserProduct(_ values: NSSet)

    @objc(removeUserProduct:)
    @NSManaged public func removeFromUserProduct(_ values: NSSet)

}
