//
//  ProductTableViewCell.swift
//  cirQR
//
//  Created by Mustafa Sabur on 24/10/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var productImage: UIImageView!
    
    
    @IBOutlet weak var brandName: UILabel!
    
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var registeredCheck: UIImageView!
    
    @IBOutlet weak var receiptCheck: UIImageView!
    
    @IBOutlet weak var warrantyCheck: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
