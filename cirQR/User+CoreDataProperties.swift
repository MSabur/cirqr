//
//  User+CoreDataProperties.swift
//  cirQR
//
//  Created by Mustafa Sabur on 09/11/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var userProduct: NSSet?

}

// MARK: Generated accessors for userProduct
extension User {

    @objc(addUserProductObject:)
    @NSManaged public func addToUserProduct(_ value: UserProduct)

    @objc(removeUserProductObject:)
    @NSManaged public func removeFromUserProduct(_ value: UserProduct)

    @objc(addUserProduct:)
    @NSManaged public func addToUserProduct(_ values: NSSet)

    @objc(removeUserProduct:)
    @NSManaged public func removeFromUserProduct(_ values: NSSet)

}
