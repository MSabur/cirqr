//
//  PopoverProductViewController.swift
//  cirQR
//
//  Created by Mustafa Sabur on 07/11/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import UIKit

class PopoverProductViewController: UIViewController {
    
    var header : String?
    var image : UIImage?
    

    @IBOutlet weak var displayTitle: UILabel!
    @IBOutlet weak var displayImage: UIImageView!{
        didSet {
            self.displayImage?.layer.cornerRadius = 8
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection();
        self.popoverPresentationController?.backgroundColor = UIColor(red:0.20, green:0.20, blue:0.20, alpha:1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
    }
    
    private func updateUI(){
        
        displayTitle.text = header
        displayImage.image = image
        
    }

}
