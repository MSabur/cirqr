//
//  SecondViewController.swift
//  cirQR
//
//  Created by Mustafa Sabur on 25/09/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import UIKit
import AVFoundation

class QRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var scanImage: UIImageView!
    @IBOutlet weak var torchSwitch: UIButton!
    @IBOutlet weak var testPop: UIButton!
    @IBOutlet weak var qrcodeImageView: UIImageView!
    
    private var popoverIsPresent = false
    private var isProcessingQR = false
    private var foundObject = false
    private var qrAnimationDidFinish = false
    
//    private var product = Product()
    
    private struct Product {
        var title: String?
        var image: UIImage?
    }
    
    private var product = Product()
    
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    // Get an instance of the AVCaptureDevice class to initialize a device object.
    let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
    
    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode]
    
    
    // MARK: - ViewController lifecycle.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createCameraLayer()
        
        view.addSubview(scanImage)
        view.bringSubview(toFront: testPop)
        view.bringSubview(toFront: torchSwitch)
        view.bringSubview(toFront: qrcodeImageView)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Check to see if there is already an animation running
        if scanImage.layer.animationKeys() == nil {
            scanImageAnimation()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setTorch(to: AVCaptureTorchMode.off)
    }
    
    
    // MARK: - Create stuff
    
    func createCameraLayer()
    {
        do {
            var captureSession:AVCaptureSession?
            
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    private func createQRImage(from text: String)
    {
        let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator"){
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("M", forKey: "inputCorrectionLevel")
            // if let outputImage = filter.outputImage {
            //  if let cgImage = CIContext(options: nil).createCGImage(outputImage, from: outputImage.extent) {
            //   qrcodeImageView.image = UIImage(cgImage: cgImage, scale: 1, orientation: UIImageOrientation.up)
            //  }
            // }
            
            // Color code and background
            if let secondFilter = CIFilter(name: "CIFalseColor"){
                secondFilter.setValue(filter.outputImage, forKey: "inputImage")
                secondFilter.setValue(CIColor.white(), forKey: "inputColor0")
                secondFilter.setValue(CIColor.clear(), forKey: "inputColor1")
                
                let transform = CGAffineTransform(scaleX: 5, y: 5)
                let transformedImage = secondFilter.outputImage!.applying(transform)
                
                qrcodeImageView.image = UIImage(ciImage: transformedImage)
                qrcodeImageView.transform = CGAffineTransform(scaleX: 0.2,y: 0.2)
                
            }
        }
    }
    
    
    // MARK: - Protocol: Gets called by the system when there is metadata found.
    
    func captureOutput( _ captureOutput: AVCaptureOutput!,
                        didOutputMetadataObjects metadataObjects: [Any]!,
                        from connection: AVCaptureConnection!)
    {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count > 0 && !popoverIsPresent && !isProcessingQR {
            // Get the metadata object.
            let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
            
            if metadataObj.type == AVMetadataObjectTypeQRCode {
                
                if metadataObj.stringValue != nil  {
                    isProcessingQR = true
                    
                    let qrcodeValue = metadataObj.stringValue!
                    tryToFindObject(withThis: qrcodeValue)
                    
                    createQRImage(from: qrcodeValue)
                    animateQRImageAndShowPopoverIfFound()
                }
            }
        }
    }
    
    private func tryToFindObject(withThis qrcode: String)
    {
        let queue = DispatchQueue.global(qos: .userInitiated)
        queue.async {
            
            self.foundObject = true
            //
            self.product.title = "Test Title yeah"
            
            if self.qrAnimationDidFinish {
                DispatchQueue.main.async {
                    self.showPopover()
                }
            }

        }
    }
    
    
    // MARK: - Action
    
    @IBAction func toggleTorch(_ sender: UIButton) {
        if (captureDevice != nil && captureDevice!.hasTorch){
            do{
                try captureDevice?.lockForConfiguration()
                if captureDevice?.torchMode == AVCaptureTorchMode.off{
                    captureDevice?.torchMode = AVCaptureTorchMode.on
                }else {
                    captureDevice?.torchMode = AVCaptureTorchMode.off
                }
                captureDevice?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    
    @IBAction func testPopover(_ sender: UIButton)
    {
        showPopover()
        
    }
    
    
    private func setTorch(to state: AVCaptureTorchMode){
        if (captureDevice != nil && captureDevice!.hasTorch){
            let currentTorchState = captureDevice?.torchMode
            
            if (state != currentTorchState){
                do{
                    try captureDevice?.lockForConfiguration()
                    captureDevice?.torchMode = state
                    captureDevice?.unlockForConfiguration()
                } catch {
                    print(error)
                }
            }
        }
    }
    

    // MARK: - Navigation
    
    private func showPopover()
    {
        if view.window != nil {
            performSegue(withIdentifier: "Popover Product", sender: self)
            qrcodeImageView.image = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "Popover Product":
                if let vc = segue.destination as? PopoverProductViewController {
                    popoverIsPresent = true
                    
                    let controller = vc.popoverPresentationController!
                    vc.modalPresentationStyle = UIModalPresentationStyle.popover
                    controller.delegate = self
                    controller.sourceView = self.view
                    controller.sourceRect = CGRect( x: view.bounds.midX,
                                                    y: view.bounds.midY,
                                                    width: view.bounds.maxX,
                                                    height: (view.bounds.maxY/2 - vc.view.bounds.maxY/2))
                    
                    vc.header = product.title
//                    if let imagedata = product.mainImage as? Data{
//                        vc.image = UIImage(data: imagedata )
//                    }
                    

                }
            default: break
            }
        }
    }
    
    
    // MARK: - Animation
    
    private func animateQRImageAndShowPopoverIfFound(){
        UIView.animate(withDuration: 1, delay: 0.0, animations: {
            self.qrcodeImageView.transform = self.qrcodeImageView.transform.rotated(by: .pi) })
        UIView.animate(withDuration: 1, delay: 0.0, animations: {
            self.qrcodeImageView.transform = CGAffineTransform(scaleX: 1.0,y: 1.0) })
        { (finished) in
            if finished {
                self.qrAnimationDidFinish = true
                if self.foundObject {
                    self.showPopover()
                }
            }
            
        }
    }
    
    private func scanImageAnimation() {
        UIView.animate( withDuration: 3, delay: 0.0 , animations: {
            self.scanImage.transform = self.scanImage.transform.rotated(by: .pi) })
        UIView.animate( withDuration: 3, delay: 0.0 , animations: {
            self.scanImage.transform = self.scanImage.transform.rotated(by: .pi) })
        { (finished) in
            if finished { self.scanImageAnimation() }
        }
    }
    
    
    // MARK: - Popover view
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        popoverIsPresent = false
        isProcessingQR = false
        foundObject = false
        qrAnimationDidFinish = false
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}

