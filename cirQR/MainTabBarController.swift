//
//  MainTabBarController.swift
//  cirQR
//
//  Created by Mustafa Sabur on 07/11/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
    }
}
