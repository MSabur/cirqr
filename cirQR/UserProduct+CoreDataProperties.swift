//
//  UserProduct+CoreDataProperties.swift
//  cirQR
//
//  Created by Mustafa Sabur on 09/11/2016.
//  Copyright © 2016 Circularise. All rights reserved.
//

import Foundation
import CoreData


extension UserProduct {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProduct> {
        return NSFetchRequest<UserProduct>(entityName: "UserProduct");
    }

    @NSManaged public var purchasedOn: NSDate?
    @NSManaged public var receipt: NSData?
    @NSManaged public var registered: Bool
    @NSManaged public var warrantyEnd: NSDate?
    @NSManaged public var product: Product?
    @NSManaged public var user: User?

}
